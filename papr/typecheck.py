from typing import List, Dict, Any


def matches_type(instance, typedef):
    """ Checks whether `instance` is an instance of `typedef`.

        `typedef` has to be a type definition as would be used for type
        annotations.
    """

    try:
        origin = typedef.__origin__
    except AttributeError:
        pass
    else:
        if origin is List:
            subt = typedef.__args__[0]
            return isinstance(instance, list) and \
                all(matches_type(x, subt) for x in instance)

        if origin is Dict:
            keyt, valt = typedef.__args__
            return isinstance(instance, dict) and \
                all(matches_type(x, keyt) for x in instance.keys()) and \
                all(matches_type(x, valt) for x in instance.values())

        if origin is Any:
            return True

        raise NotImplementedError

    return isinstance(instance, typedef)
