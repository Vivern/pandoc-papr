import papr
import sys
import argparse
from pathlib import Path
import toml
import re
from typing import List, Dict
from functools import reduce
import os
import math


def parse_path(path, name, rel_to=Path('.'), ensure: str=None):
    """ Ensures `path` is a string containing a file path. If so returns the
        Path object corresponding to it. Otherwise a UserError is thrown.

        @param path: the unparsed path
        @param name: A short name describing what `path` is. Used in the error message.
        @param rel_to:
        @param ensure: Can be used to check that the path fulfills certain requirements
                          - `None`: don't check anything
                          - 'exists': the path exists
                          - 'dir': the path points to a directory
                          - 'file': the path points to a regular file
    """
    try:
        res = Path(path)
    except TypeError as err:
        raise papr.UserError(f'{name} is not a valid path') from err

    res = rel_to / res

    if ensure is not None:
        if not res.exists():
            raise papr.UserError(f'The {name} at {res} does not exist')

        if ensure == 'dir':
            if not res.is_dir():
                raise papr.UserError(f'The {name} at {res} has to be a directory')

        elif ensure == 'file':
            if not res.is_file():
                raise papr.UserError(f'The {name} at {res} has to be a regular file')

    else:
        assert ensure is None, ensure

    return res


def natural_sort(values, key=lambda x: x):
    # adapted from
    # https://stackoverflow.com/questions/4836710/does-python-have-a-built-in-function-for-string-natural-sort

    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda x: [ convert(c) for c in re.split('([0-9]+)', key(x)) ] 
    return sorted(values, key=alphanum_key)


def replace_all(string: str, mapping: Dict[str, str]):
    """ Replaces all occurrences of mapping.keys() in `string` with their 
        corresponding values.

        Replacement is currently done in multiple passes, so 
          - later (key, value) pairs may replace previously replaced strings
          - the exact result depends on the order of the keys inside the
            dictionary
    """

    return reduce(
        lambda accu, rep: accu.replace(*rep),
        mapping.items(),
        string
    )


def parse_sources(sources, sources_root: Path):
    if not isinstance(sources, (list, tuple)):
        raise papr.UserError('The document sources have to be a list of glob patterns')

    result = []

    for pattern in sources:
        if not isinstance(pattern, str):
            raise papr.UserError('The document sources have to be glob patterns')

        matches = tuple(sources_root.glob(pattern))
        if not matches:
            print(f'Warning: The pattern "{pattern}" did not match any source files"')

        # The source files _have_ to be placed inside `sources-root`. Supporting
        # outside files makes cache handling much harder
        #
        # There doesn't appear to be a good solution for checking this though
        abs_rel_to_str = str(sources_root.resolve())
        for match in matches:
            if not str(match.resolve()).startswith(abs_rel_to_str):
                err_path = match.resolve().relative_to(Path.cwd())
                raise papr.UserError(f'''Source files have to be placed inside \
'sources-directory'. Please move {err_path} into {sources_root} or adjust the  \
sources specified in the paper's TOML.''')

        # sort the matches to get a well-defined order
        matches = natural_sort(
            matches,
            lambda x: x.name
        )

        result += matches

    return result

    
def parse_args():
    # define the arguments
    parser = argparse.ArgumentParser(
        description='Compile document definitions'
    )

    parser.add_argument(
        'input',
        type=str,
        help='the TOML file containing the document definition',
    )

    parser.add_argument(
        'output',
        type=str,
        help='where to store the result',
    )

    parser.add_argument(
        '--build-cache',
        dest='build_cache',
        type=str,
        help='directory used to store intermediate build files (relative to the document TOML)',
        default='.papr_cache'
    )

    parser.add_argument(
        '--separate',
        dest='separate',
        help='''Convert each input file individually instead of merging them. \
                This option takes as argument the pattern to use for          \
                generating the output path for each file.                     \
                Use $(reldir) $(stem) $(suffix) $(name) $(index)''',
        nargs='?',
        default=False,
    )

    # parse and verify the results
    args = parser.parse_args()

    args.input = parse_path(
        args.input,
        'document definition TOML',
        ensure='file'
    )

    args.output = parse_path(
        args.output,
        'output file',
    )

    args.build_cache = parse_path(
        args.build_cache,
        'build cache TOML',
    )

    return args


def parse_paper(args: 'argparse.Namespace'):
    # read the toml
    doc_toml = toml.load(args.input.open())

    # parse the root directory for all document sources
    try:
        sources_dir = doc_toml.pop('sources-directory')
    except KeyError:
        sources_dir = args.input.parent
    else:
        sources_dir = parse_path(
            sources_dir,
            'sources-directory',
            args.input.parent,
            'dir'
        )

    # parse the document sources
    sources = parse_sources(
        doc_toml.pop('sources', []),
        sources_dir
    )

    # get the template, if any
    try:
        template = doc_toml.pop('template')
    except KeyError:
        template = None
    else:
        template = parse_path(
            template,
            'template',
            args.input.parent,
            'file'
        )

    # parse the additional pandoc arguments
    pandoc_arguments = doc_toml.pop('pandoc-arguments', [])

    # check their type
    if not isinstance(pandoc_arguments, list):
        raise papr.UserError('"pandoc-arguments" has to be a list of strings')

    if pandoc_arguments and any(not isinstance(x, str) for x in pandoc_arguments):
        raise papr.UserError('The additional arguments to pandoc have to be strings')

    # expand the variables
    pandoc_arg_variables = {
        '$(TOML_DIR)': str(args.input.parent.resolve()),
    }
    pandoc_arguments = \
        [ replace_all(x, pandoc_arg_variables) for x  in pandoc_arguments]


    # warn when superfluous keys exist in the TOML
    for key in doc_toml.keys():
        print(f'Warning: Ignoring unrecognized option "{key}" in the document definition')


    # construct the Paper instance
    paper = papr.Paper(
        sources_dir,
        sources,
        pandoc_arguments,
    )

    return paper


def convert_individual(
        sources: List[Path],
        sources_dir: Path,
        output_root: Path,
        new_suffix: str,
        pandoc_arguments: List[str]):

    result_paths = []

    for source in sources:
        # determine the target file location
        opath = output_root / source.relative_to(sources_dir)
        opath = opath.with_suffix(new_suffix)
        result_paths.append(opath)

        # ensure the target directory exists
        # This has to be done for each file, because they may be placed in their
        # own subdirectories
        opath.parent.mkdir(parents=True, exist_ok=True)

        print(f'Converting {source}...')

        # convert
        papr.pandoc.convert(
            [source],
            opath,
            pandoc_arguments
        )

    return result_paths


def compile_separate(paper, args):
    index_padding = math.ceil(math.log10(len(paper.sources) + 1))

    for ii, source in enumerate(paper.sources):
        # determine the target file location
        pattern_vars = {
            '$(reldir)': str(source.parent.relative_to(paper.sources_dir)),
            '$(name)':   source.name,
            '$(stem)':   source.stem,
            '$(suffix)': source.suffix,
            '$(index)':  str(ii + 1).zfill(index_padding),
        }
        opath = args.output / Path(replace_all(args.separate, pattern_vars))

        # ensure the target directory exists
        # This has to be done for each file, because they may be placed in their
        # own subdirectories
        opath.parent.mkdir(parents=True, exist_ok=True)

        print(f'Converting {source}...')

        # convert
        papr.pandoc.convert(
            [source],
            opath,
            paper.pandoc_arguments
        )


def compile_paper(paper, args):
    # The input sources may use different file formats. This is not supported
    # by pandoc, so to circumvent this restriction first convert each chapter
    # to a common format -> JSON
    json_paths = convert_individual(
        paper.sources,
        paper.sources_dir,
        args.build_cache / 'json',
        '.json',
        paper.pandoc_arguments
    )

    # merge the previously generated JSONs
    print('Merging files...')
    papr.pandoc.convert(
        json_paths,
        args.output,
        paper.pandoc_arguments
    )

    print('Done!')


def main():
    args = parse_args()

    paper = parse_paper(args)

    if args.separate is not False:
        compile_separate(paper, args)
    else:
        compile_paper(paper, args)


if __name__ == '__main__':
    try:
        main()
    except papr.UserError as err:
        print(f'Error: {err}')
        sys.exit(1)
