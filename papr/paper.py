from typing import List
from pathlib import Path


class Paper:
    def __init__(self,
        sources_dir: Path,
        sources: List[Path],
        pandoc_arguments: List[str]):

        self.sources_dir = sources_dir
        self.sources = sources
        self.pandoc_arguments = pandoc_arguments

