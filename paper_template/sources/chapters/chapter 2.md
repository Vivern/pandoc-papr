# Document Syntax

One of the most common markup languages is
[Markdown](https://daringfireball.net/projects/markdown/). It is a simple,
human-readable language that can be converted into a variety of other document
formats, including PDFs, websites and even e-books. This makes it well suited
for any writing project.

This chapter contains some of the most common concepts you will use in markdown.
To see the actual markdown source take a peek at `sources/chapters/chapter 2.md`.

## Giving the document structure

Headings are denoted by placing a hash sign in front of them. The number of
hash signs controls the level of they heading.

Placing two of them makes a sub-heading, three a sub-sub-heading and so on. You
can go up to six levels deep!

Linebreaks by themselves are ignored.
This line is in the same paragraph as the one above.

This
is
only
one
_awesome_
paragraph!

You can use this property to style your source files any way you please. To make
an actual paragraph use a blank line.

## Making things pop

This is how you make text _italic_, **bold** or `monospace`.

Lists can be made by placing bullets in front of the items

  * FIRST!
  * Second
  * Third

Numbered lists work in a similar fashion:

  1. FIRST AGAIN!
  2. Second
  3. Third

## Adding Content

Markdown supports hyperlinks. [This one will take you to pandoc's markdown user
guide](https://pandoc.org/MANUAL.html#pandocs-markdown).

Images work just like hyperlinks, but have an exclamation mark in front
![Markdown mark](Markdown-mark.svg)

If `bibtex-db` is specified in the document's TOML file citations are supported
as well [@adams2007hitchhiker].

These are the most important concepts of markdown. If you ever need something
just look it up as you need it!

One last thing: Because papr uses `pandoc` you have access to an even better
version of markdown. See all of the goodies
[here](https://pandoc.org/MANUAL.html#pandocs-markdown).
