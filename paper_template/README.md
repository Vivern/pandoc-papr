# Welcome to papr!

papr is a tool that helps you write professional, clean looking papers using
languages you already know, like Markdown.

This folder contains a simple setup that contains everything you need to get
started writing. Place your source files in `/sources` and any images or other
resources you'd like to include into `/resources`.

`paper.toml` controls how the document is created. All options have helpful
comments that tell you everything you need to know. Go take a look!

You can create the document by running this command:

```
papr /path/to/paper.toml paper.pdf
```

This will create a file called `paper.pdf`. It contains a short introduction
into markdown as well links to more resources in case you get stuck.

Happy writing!
